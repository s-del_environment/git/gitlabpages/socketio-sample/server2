import crypto = require('crypto');

export class UserID {
  private readonly id: string;

  constructor(ip: string) {
    this.id = this.createID(ip);
  }

  readonly createID = (ip: string): string => {
    return crypto.createHash('sha256')
                 .update(`${ip}`, 'utf8')
                 .digest('base64');
  };

  readonly getID = (): string => this.id.slice(0, 11);
}
