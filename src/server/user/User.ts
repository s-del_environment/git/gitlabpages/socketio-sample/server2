import { UserName } from "./UserName";
import { UserID } from "./UserID";

export class User {
  private readonly name: UserName;
  private readonly id: UserID;
  lastSendTime: number;

  constructor(ip: string) {
    this.name = new UserName();
    this.id = new UserID(ip);
    this.lastSendTime = 0;
  }

  readonly setName = (newName: string): void => this.name.setName(newName);

  readonly displayName = (): string => {
    return `${this.name.getName()} (${this.id.getID()})`;
  };
}
