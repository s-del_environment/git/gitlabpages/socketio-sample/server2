import SocketIO = require('socket.io');
import { User } from '../user/User';

export class SendMessageEvent implements IServerEvent {
  private readonly io: SocketIO.Server;
  private readonly socket: SocketIO.Socket;
  private readonly users: Map<string, User>;
  private readonly eventName: RequestEventName;

  constructor(
    io: SocketIO.Server,
    socket: SocketIO.Socket,
    userMap: Map<string, User>,
    eventName: RequestEventName
  ) {
    this.io = io;
    this.socket = socket;
    this.users = userMap;
    this.eventName = eventName;
  }

  readonly listen = (): void => {
    this.socket.on(this.eventName, (message: unknown) => {
      if (typeof message !== 'string') return;
      if (message.length > 140) return;
      if (/^\s+$/.test(message)) return;

      const user = this.users.get(this.socket.id);
      if (!user) return;

      const now = Date.now();
      if (now - user.lastSendTime < 1200) return;
      user.lastSendTime = now;

      const newMessage = message.trim().replace(/\s+/g, ' ');
      console.log(`${user.displayName()}: ${newMessage}`);
      this.io.emit('response_message', user.displayName(), newMessage);
    });
  };
}
