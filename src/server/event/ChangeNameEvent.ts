import SocketIO = require('socket.io');
import { User } from '../user/User';

export class ChangeNameEvent implements IServerEvent {
  private readonly socket: SocketIO.Socket;
  private readonly users: Map<string, User>;
  private readonly eventName: RequestEventName;

  constructor(
    socket: SocketIO.Socket,
    userMap: Map<string, User>,
    eventName: RequestEventName
  ) {
    this.socket = socket;
    this.users = userMap;
    this.eventName = eventName;
  }

  readonly listen = (): void => {
    this.socket.on(this.eventName, (newName: unknown) => {
      if (typeof newName !== 'string') return;
      if (newName.length > 10) return;
      if (/^\s+$/.test(newName)) return;

      const user = this.users.get(this.socket.id);
      if (!user) return;

      const now = Date.now();
      if (now - user.lastSendTime < 1200) return;
      user.lastSendTime = now;

      const currentName = user.displayName();
      user.setName(newName.trim().replace(/\s+/g, ' '));
      console.log([
        `${this.socket.handshake.address}`,
        'change name:',
        `${currentName} -> ${user.displayName()}`
      ].join(' '));
      this.socket.emit('update_name', user.displayName());
    });
  };
}
