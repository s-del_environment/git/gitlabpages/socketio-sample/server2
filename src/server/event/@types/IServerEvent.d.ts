declare interface IServerEvent {
  readonly listen(): void;
}
