import SocketIO = require('socket.io');
import { User } from '../../user/User';
import { AbstractEventFactory } from './AbstractEventFactory';
import { ConnectionEvent } from '../ConnectionEvent';
import { ChangeNameEvent } from '../ChangeNameEvent';
import { SendMessageEvent } from '../SendMessageEvent';
import { DisconnectEvent } from '../DisconnectEvent';

export class EventFactory extends AbstractEventFactory {
  private readonly events: Map<RequestEventName, IServerEvent>;

  constructor(
    io: SocketIO.Server,
    socket: SocketIO.Socket,
    userMap: Map<string, User>
  ) {
    super();
    this.events = new Map<RequestEventName, IServerEvent>();
    this.events.set('connection', new ConnectionEvent(socket, userMap));
    this.events.set(
      'change_name',
      new ChangeNameEvent(socket, userMap, 'change_name')
    );
    this.events.set(
      'send_message',
      new SendMessageEvent(io, socket, userMap, 'send_message')
    );
    this.events.set(
      'disconnect',
      new DisconnectEvent(socket, userMap, 'disconnect')
    );
  }

  protected readonly createEvent = (
    eventName: RequestEventName
  ): IServerEvent | null => this.events.get(eventName) || null;
}
